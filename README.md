FastEmbedSpark
==============

PySpark implementation of the NIPS 2015 paper ["Compressive spectral embedding: sidestepping the SVD," a.k.a FastEmbed](http://papers.nips.cc/paper/5992-compressive-spectral-embedding-sidestepping-the-svd)

A single machine multi-core version is available [here](https://bitbucket.org/dineshkr/fastembed/src)


# Overview

This package provides means to compute approximate SVD embeddings in the following settings:

* High dimensional data and the need for SVD embeddings that include the effect of a large number of singular vectors can make conventional SVD embedding computation expensive
* The cost function for downstream inference methods can be shown to depend on data samples only via pairwise euclidean distances between data samples or metrics derived from these pairwise distances like the inner products, cosine similarity, etc. e.g, Support Vector Machines, k-means, etc
* These downstream inference methods are robust to small distortions in pairwise distances among data samples introduced by compressive embeddings


# Embedding Large Graphs

## Undirected graphs

Suppose that an undirected graph is available in `hdfs-path`. Each entry in `hdfs-path` is a csv line like so: `nodeId1,nodeId2,edgeWeight`. Concrete steps are as follows:

* Make required imports
* Create an list of weighted edges using an `RDD` of the form `(u, v, w)` representing an weighted edge of weight `w` between `u` and `v`
* Create an `EdgesRdd` instance from this list of weighted edges so that we can ensure that the matrix is symmetric
* Create an `AdjListRDD` from this `EdgesRdd` instance
* [Normalize this adjacency matrix](https://en.wikipedia.org/wiki/Laplacian_matrix)
* Compute the embedding of the graph. The spectral cutoff 0.95 is a hyper-parameter that must be tuned on a per graph basis (in the range (-1, 1))
* Each entry of the final rdd is a tuple of the form `(nodeId, 40 dimensional embedding of nodeId)`

```python
from fast_embed.api import fast_embed_eig
from fast_embed.linalg.algorithms import normalize_adj_matrix
from fast_embed.linalg.datastructures import EdgesRDD, AdjListRDD

uvwRdd = sc.textFile("hdfs-path", sc.defaultParallelism).\
    map(lambda x: x.split(',')).\
    map(lambda x: (x[0], x[1], float(x[2])))

edgesRdd = EdgesRDD().fromUvw(uvwRdd).ensureSymmetric()
adjListRdd = AdjListRDD().fromEdgesRDD(edgesRDD=edgesRdd)

normedAdjListRdd = normalize_adj_matrix(adjListRdd)

fun = lambda x: 1. if x > 0.95 else 0.
config = {
    "poly_order": 40,
    "boost": 2,
    "sigma_max": 1,
    "verbose": True,
    "checkpoint": True,
    "checkpoint_dir": "hdfs-checkpoint-directory"
}
embedRdd = fast_embed_eig(coo_list=normedAdjListRdd.toUvw(), fun=fun, embed_dims=40, config=config)
```


## Directed graphs

Suppose that an directed graph is available in `hdfs-path`. Each entry in `hdfs-path` is a csv line like so: `nodeId1,nodeId2,edgeWeight` and represents a weighted edge from `nodeId1` to `nodeId2`. Concrete steps are as follows:
 
* Make required imports
* Create an list of weighted edges using an `RDD` of the form `(from:u, to:v, w)` representing an weighted edge of weight `w` between `u` and `v`. We need unique names for rows and columns to represent directed edges. So we rename `u` as `from:u` and `v` as `to:v`.
* Create an `AdjListRDD` from list of edges `RDD`
* Create a random walk transition probability matrix out of this `AdjListRDD`
* Compute the embedding of this matrix. The spectral cutoff 0.95 is a hyper-parameter that must be tuned on a per graph basis (in the range (0, 1))
* Each entry of `embedRowsRdd` is a tuple of the form `(from:nodeId, 40 dimensional embedding of from:nodeId)` and `embedColsRdd` is of the form `(to:nodeId,  40 dimensional embedding of to:nodeId)`. It is recommended that we union the `from:nodeId` and `to:nodeId` embeddings to obtain an 80-dimensional embedding of each `nodeId`.
* Each entry of the final rdd is a tuple of the form `(nodeId, 80 dimensional embedding of nodeId)`

```python
from fast_embed.api import fast_embed_svd
from fast_embed.linalg.algorithms import rw_probability_transition_matrix
from fast_embed.linalg.datastructures import AdjListRDD
import numpy as np

uvwRdd = sc.textFile("hdfs-path", sc.defaultParallelism).\
    map(lambda x: x.split(',')).\
    map(lambda x: ("from:{}".format(x[0]), "to:{}".format(x[1]), float(x[2])))

adjListRdd = AdjListRDD().fromUvw(uvwRDD=uvwRdd)

rwProbTransMatRdd = rw_probability_transition_matrix(adjListRdd)

fun = lambda x: 1. if abs(x) > 0.95 else 0.

config = {
    "poly_order": 40,
    "boost": 2,
    "verbose": True,
    "checkpoint": True,
    "checkpoint_dir": "hdfs-checkpoint-directory"
}

embedRowsRdd, embedColsRdd = fast_embed_svd(rwProbTransMatRdd.toUvw(), fun=fun, embed_dims=40, config=config)

embedRdd = embedRowsRdd.map(lambda x: (x[0].split(":")[1], x[1])).join(
    embedColsRdd.map(lambda x: (x[0].split(":")[1], x[1]))
).mapValues(lambda x: np.append(*x))
```

## More Examples

See test_scripts/ for examples including co-occurrence based word embeddings

# Citing

If you find FastEmbed useful for your research, please consider citing the paper

```
@inproceedings{fastembed-nips2015,
author = {Ramasamy, Dinesh and Madhow, Upamanyu},
 title = {Compressive spectral embedding: sidestepping the SVD},
 booktitle = {Neural Information Processing Systems},
 year = {2015}
}

```

# Setup

On a spark driver node:

* virtualenv venv
* source venv/bin/activate
* cd fastembedspark/
* pip install -r requirements.txt
* python setup.py install
* Follow the examples in fastembedspark/test_scripts to write your spectral graph filtering pipeline


# Note

This project has been set up using PyScaffold 2.5.7. For details and usage information on PyScaffold see http://pyscaffold.readthedocs.org/.
