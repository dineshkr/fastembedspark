#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

spark-submit \
    --master "local" \
    --conf "spark.executor.extrajavaoptions=-Xmx1024m" \
    --conf "spark.executor.memory=3g" \
    --conf "spark.executor.cores=3" \
    --conf "spark.driver.memory=1g" \
    --conf "spark.default.parallelism=10" \
    ${DIR}/test_e2e.py -n 5000 -m 10000 --num_embed 80 --normalize | tee ${DIR}/test_e2e.log

