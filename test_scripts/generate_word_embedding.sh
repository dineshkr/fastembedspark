#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

spark-submit \
    --master "local" \
    --conf "spark.executor.extrajavaoptions=-Xmx1024m" \
    --conf "spark.executor.memory=3g" \
    --conf "spark.executor.cores=4" \
    --conf "spark.driver.memory=1g" \
    --conf "spark.default.parallelism=10" \
    ${DIR}/generate_word_embedding.py -f ${DIR}/resources/snippets.txt \
            -o ${DIR}/word_embedding.csv --num_embed 40 | tee ${DIR}/test_embedding.log

