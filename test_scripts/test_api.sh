#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd ${DIR}/resources
tar -xzvf ${DIR}/resources/email-Eu-core.tar.gz
popd;

spark-submit \
    --master "local" \
    --conf "spark.executor.extrajavaoptions=-Xmx1024m" \
    --conf "spark.executor.memory=3g" \
    --conf "spark.executor.cores=3" \
    --conf "spark.driver.memory=1g" \
    --conf "spark.default.parallelism=10" \
    ${DIR}/test_api.py -f ${DIR}/resources/email-Eu-core.txt --num_embed 80 --normalize | tee ${DIR}/test_api.log

rm ${DIR}/resources/email-Eu-core.txt
