import math
import os
import re
from argparse import ArgumentParser
from operator import add

from pyspark import SparkContext

from fast_embed.api import fast_embed_svd
from fast_embed.linalg.datastructures import EdgesRDD, AdjListRDD


def parse_args():
    parser = ArgumentParser(description="Test PySpark implementation of FastEmbed using ord embeddings")
    parser.add_argument('-f', '--file', help='File with sentences', required=True, type=str)
    parser.add_argument('-o', '--output_file', help='File with word embeddings', required=True, type=str)
    parser.add_argument('--num_embed', help='Number of embed dims', required=False, type=int, default=100)
    return parser.parse_args()


def context_matrix(sc, fname, hdfs=False):
    """
    Generate u,v,w tuples of word frequencies (u=word, v=context word, w=positive pointwise mutual information)
    :param sc: Spark context
    :param fname: File name. Each line corresponds to a sentence
    :param hdfs: is the file local or on hdfs?
    :return:
    """
    import math

    def form_tuples(words):
        return [
            ((word_i, word_j), 1. / abs(i - j))
            for i, word_i in enumerate(words) for j, word_j in enumerate(words)
            if i != j and word_i != word_j and abs(i - j) <= 5
        ]

    linesRdd = sc.textFile(fname) if hdfs else sc.parallelize([line for line in open(fname)])

    counts = linesRdd. \
        map(lambda line: re.sub(r'\W+', ' ', line)). \
        map(lambda line: map(lambda word: word.lower().strip(), line.split())). \
        flatMap(form_tuples). \
        reduceByKey(add). \
        map(lambda kv: (kv[0], math.pow(kv[1], 0.75)))

    total_count = counts.values().sum()

    counts_words = counts.map(lambda kv: (kv[0][0], kv[1])).reduceByKeyLocally(add)
    counts_context = counts.map(lambda kv: (kv[0][1], kv[1])).reduceByKeyLocally(add)

    return counts.\
        map(lambda kv: (kv[0], math.log(kv[1] * total_count / counts_words[kv[0][0]] / counts_context[kv[0][1]]))).\
        filter(lambda kv: kv[1] > 0).\
        map(lambda kv: (kv[0][0], kv[0][1], kv[1]))


def output_embedding(fname, embeddingRdd):
    with open(fname, 'w') as fout:
        for word, embedding_vec in embeddingRdd.collect():
            _, word = word.split(':')
            line = word + "," + ",".join(map(str, embedding_vec.tolist()))
            fout.write(line + "\n")


def main():
    sc = SparkContext()
    args = parse_args()
    embed_dims = args.num_embed

    uvwRdd = context_matrix(sc, args.file)

    uvw = EdgesRDD().fromUvw(uvwRdd)
    mat = AdjListRDD().fromEdgesRDD(uvw)

    def fun(x):
        return 1. if abs(x) > 0.01 else 0.

    def phi(x):
        return 1. / math.sqrt(1. + 1e-2 - x * x)

    config = {
        'spec_norm_est_iter': 25,
        'poly_order': 20,
        'scale_up_spec_norm_est': 1.0005,
        'boost': 2,
        'checkpoint': True,
        'checkpoint_dir': os.path.join(os.path.dirname(__file__), 'resources', 'checkpoints')
    }

    embed_rows, embed_cols = fast_embed_svd(
        mat.toUvw().map(lambda x: ('Row:{}'.format(x[0]), 'Column:{}'.format(x[1]), x[2])),
        fun=fun,
        embed_dims=embed_dims,
        config=config,
        phi=phi
    )

    print("Example: " + str(embed_rows.first()))
    output_embedding(args.output_file, embed_rows)


if __name__ == '__main__':
    main()
