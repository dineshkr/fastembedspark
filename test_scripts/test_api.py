import math
import os
from argparse import ArgumentParser

from pyspark import SparkContext

from fast_embed.api import fast_embed_svd
from fast_embed.linalg.algorithms import normalize_adj_matrix
from fast_embed.linalg.datastructures import EdgesRDD, AdjListRDD


def parse_args():
    parser = ArgumentParser(description="Test PySpark implementation of FastEmbed")
    parser.add_argument('-f', '--file', help='File with edges', required=False, type=str, default=None)
    parser.add_argument('--num_embed', help='Number of embed dims', required=False, type=int, default=100)
    parser.add_argument('--normalize', dest='normalize', action='store_true')
    parser.set_defaults(normalize=False)
    return parser.parse_args()


def main():
    sc = SparkContext()
    args = parse_args()
    embed_dims = args.num_embed
    if len(open(args.file).next().split()) == 2:
        u, v = zip(*[line.split() for line in open(args.file)])
        d = [1.] * len(u)
    else:
        u, v, d = zip(*[line.split() for line in open(args.file)])
        d = map(float, d)
    u = map(int, u)
    v = map(int, v)

    uvw = EdgesRDD().fromUvw(
        sc.parallelize(zip(u, v, d)).filter(lambda x: x[0] != x[1])
    )
    mat = AdjListRDD().fromEdgesRDD(uvw)

    def fun(x):
        return 1. * (x > 0.2)

    def phi(x):
        return 1. / math.sqrt(1. + 1e-2 - x * x)

    config = {
        'spec_norm_est_iter': 41,
        'poly_order': 61,
        'scale_up_spec_norm_est': 1.0005,
        'boost': 2,
        'checkpoint': True,
        'checkpoint_dir': os.path.join(os.path.dirname(__file__), 'resources', 'checkpoints')
    }

    if args.normalize:
        mat = normalize_adj_matrix(mat)

    embed_rows, embed_cols = fast_embed_svd(
        mat.toUvw().map(lambda x: ('Row:{}'.format(x[0]), 'Column:{}'.format(x[1]), x[2])),
        fun=fun,
        embed_dims=embed_dims,
        config=config,
        phi=phi
    )

    print(embed_rows.count())


if __name__ == '__main__':
    main()
