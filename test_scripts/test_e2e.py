import math
import random
import os.path
from argparse import ArgumentParser
from operator import add

from pyspark import SparkContext

from fast_embed.fast_embed import FastEmbed
from fast_embed.linalg.algorithms import normalize_adj_matrix
from fast_embed.linalg.datastructures import AdjListRDD, EdgesRDD


def parse_args():
    parser = ArgumentParser(description="Test PySpark implementation of FastEmbed")
    parser.add_argument('-n', '--num_dims', help='Number of rows', required=False, type=int, default=None)
    parser.add_argument('-m', '--num_edges', help='Number of undirected edges', required=False, type=int, default=None)
    parser.add_argument('-f', '--file', help='File with edges', required=False, type=str, default=None)
    parser.add_argument('--num_embed', help='Number of embed dims', required=False, type=int, default=100)
    parser.add_argument('--normalize', dest='Use normalized adjacency matrix', action='store_true')
    parser.set_defaults(normalize=False)
    return parser.parse_args()


def main():
    sc = SparkContext()
    args = parse_args()
    embed_dims = args.num_embed
    if args.file is not None:
        if len(open(args.file).next().split()) == 2:
            u, v = zip(*[line.split() for line in open(args.file)])
            d = [1.] * len(u)
        else:
            u, v, d = zip(*[line.split() for line in open(args.file)])
            d = map(float, d)
        u = map(int, u)
        v = map(int, v)
    elif args.num_edges is not None and args.num_dims is not None:
        m = args.num_edges
        n = args.num_dims
        u, v, d = zip(*[(i % n, int(math.floor(random.random() * n)), 1.) for i in range(m)])
    else:
        raise IOError("Supply num_dims or file as arguments to this script")

    u1 = u + v
    v1 = v + u
    d1 = d + d

    uvw = EdgesRDD().fromUvw(
        sc.parallelize(zip(u1, v1, d1))
    )

    mat = AdjListRDD().fromEdgesRDD(uvw)

    if args.normalize:
        mat = normalize_adj_matrix(mat)

    def fun_1(x):
        return 1. * (x > -0.3)

    def fun_2(x):
        return 1. * (x > 0.6)

    def phi(x):
        return 1. / math.sqrt(1. + 1e-2 - x * x)

    config_1 = {
        'spec_norm_est_iter': 41,
        'poly_order': 61,
        'scale_up_spec_norm_est': 1.0025,
        'boost': 3,
        'checkpoint': True,
        'checkpoint_dir': os.path.join(os.path.dirname(__file__), 'resources', 'checkpoints')
    }

    config_2 = {
        'spec_norm_est_iter': 31,
        'poly_order': 41,
        'scale_up_spec_norm_est': 1.005,
        'boost': 2
    }

    fast_embed_1 = FastEmbed(mat, fun_1, embed_dims, phi=phi, config=config_1)
    fast_embed_2 = FastEmbed(mat, fun_2, embed_dims, phi=phi, config=config_2)

    # sanity check on energy in the two embeddings using the fact that fun_1
    # captures more eigenvectors (is a superset) than fun_2
    sum_1 = fast_embed_1.embed._rdd.values().map(lambda x: (x * x).sum()).reduce(add) / embed_dims
    sum_2 = fast_embed_2.embed._rdd.values().map(lambda x: (x * x).sum()).reduce(add) / embed_dims
    print ("Sum of embedding weights in hopefully descending order {}, {}".format(sum_1, sum_2))

    assert sum_1 > sum_2


if __name__ == '__main__':
    main()
