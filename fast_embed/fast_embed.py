import logging
import sys
import time

import numpy as np

from linalg.algorithms import compute_embed, principal_component
from linalg.datastructures import AdjListRDD
from polynomials.polynomial_approximator import PolynomialApproximator

logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
loglevel = logging.DEBUG
logging.basicConfig(level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


class FastEmbed:
    """
    Class that contains the compressed embedding obtained using the FastEmbed algorithm and the associated parameters
    """
    BOOST = 2
    POLY_ORDER = 100

    @staticmethod
    def _default_phi(_):
        return 1.

    SCALE_UP_SPEC_NORM_EST = 1.02
    SPEC_NORM_EST_ITER = 40
    NORMALIZE_FUN = True
    VERBOSE = True
    CHECKPOINT = False
    CHECKPOINT_DIR = ''
    CONFIG_INPUT = {'spec_norm_est_iter', 'poly_order', 'scale_up_spec_norm_est',
                    'sigma_max', 'boost', 'normalize_fun', 'verbose', 'checkpoint',
                    'checkpoint_dir'}

    def __init__(self, mat, fun, embed_dims, config=None, phi=None):
        """
        Embedding object for symmetric matrices
        :param mat: A symmetric sparse matrix in Adj list format (RDD)
        :param fun: Spectral weighting function used to weight eigenvectors
        :param embed_dims: Number of dims for the embedding
        :param phi: Approximation error weighting function
        :param config: (Optional) Dict with algorithm configuration parameters
        :return: FastEmbed object; Find embedding in obj.embed._rdd
        """

        if not isinstance(mat, AdjListRDD):
            raise ValueError("Invalid input type")

        self.mat = mat
        self.spark_context = mat._rdd.context
        # number of rows
        self.n = mat.size()[0]
        self.fun = fun
        self.normalize_fun = FastEmbed.NORMALIZE_FUN
        self.embed_dims = embed_dims
        if phi is None:
            self.phi = FastEmbed._default_phi
        else:
            self.phi = phi

        self.poly_order = FastEmbed.POLY_ORDER
        self.boost = FastEmbed.BOOST

        self.spec_norm_est_iter = FastEmbed.SPEC_NORM_EST_ITER
        self.scale_up_spec_norm_est = FastEmbed.SCALE_UP_SPEC_NORM_EST
        self.sigma_max = None
        self.verbose = FastEmbed.VERBOSE
        self.checkpoint = FastEmbed.CHECKPOINT
        self.checkpoint_dir = FastEmbed.CHECKPOINT_DIR

        if config is not None:
            self._load_config(config)

        if self.checkpoint:
            self.spark_context.setCheckpointDir(self.checkpoint_dir)

        if self.sigma_max is None:
            self._est_sigma_max()

        self.poly_approx = self._approx_poly(fun, self.boost, self.sigma_max, self.normalize_fun, self.phi,
                                             self.poly_order)

        if self.verbose:
            print "Spectral norm of matrix is {0}".format(self.sigma_max)
            print "Polynomial approximation error is {0}".format(self.poly_approx.approx_error)
            print "Proceeding to compute embedding ..."
            logging.log(level=loglevel, msg="Lanczos polynomial series recursion alpha {} beta {}".format(
                self.poly_approx.lanczos_series.alpha,
                self.poly_approx.lanczos_series.beta,
            ))
            logging.log(level=loglevel, msg="Lanczos mixing weights {}".format(self.poly_approx.lanczos_coeff))

        self.proj_mat = None
        self.embed = None
        # Fill up self.embed
        self._eval_embed()

    def _load_config(self, config):
        """
        Loads parameters specified via the config dict
        """

        for key in FastEmbed.CONFIG_INPUT:
            if key in config:
                setattr(self, key, config[key])

    @staticmethod
    def _approx_poly(fun, boost, sigma_max, normalize_fun, phi, poly_order):
        """
        Sets up the polynomial approximation of the spectral weighting function fun using the relevant parameters
        """
        if boost < 1:
            raise ValueError('Boost parameter cannot be smaller than 1')
        if normalize_fun:
            eff_fun = fun if boost == 1 else \
                lambda x: np.power(np.abs(fun(x)), 1. / boost) * np.sign(fun(x))
        else:
            eff_fun = lambda x: fun(1. * x / sigma_max) if boost == 1 else \
                lambda x: np.power(np.abs(fun(1. * x / sigma_max)), 1. / boost) * np.sign(
                    fun(1. * x / sigma_max))
        return PolynomialApproximator(eff_fun, phi, poly_order)

    def _est_sigma_max(self):
        """
            Spectral norm estimation via Power Iteration
        """

        logging.debug(msg="Estimating spectral norm")
        sigma_max, _, _ = principal_component(self.mat, int(np.ceil(self.spec_norm_est_iter / 2.)),
                                              checkpoint=self.checkpoint)
        logging.debug(msg="Lower bound on spectral norm is {}".format(sigma_max))
        self.sigma_max = self.scale_up_spec_norm_est * float(sigma_max)
        logging.debug(msg="Setting spectral norm to {}".format(self.sigma_max))

    def _eval_embed(self):
        """
        Sets up a parallel implementation of FastEmbed
        """
        logging.debug(msg="Computing embedding using lambda_max = {}, ndims = {}, boost = {}, approx order = {}".format(
            self.sigma_max, self.embed_dims, self.boost, self.poly_approx.order))

        tstart = time.time()
        proj_mat, embed = compute_embed(
            mat=self.mat,
            mat_spec_norm=self.sigma_max,
            embed_dims=self.embed_dims,
            boost=self.boost,
            lanczos_coeff=self.poly_approx.lanczos_coeff,
            alpha=self.poly_approx.lanczos_series.alpha,
            beta=self.poly_approx.lanczos_series.beta,
            checkpoint=self.checkpoint
        )
        tend = time.time()

        self.proj_mat = proj_mat
        self.embed = embed
        logging.debug(msg="Computed embedding in {} minutes".format((tend - tstart) / 60.))
