from fast_embed import FastEmbed
from linalg.datastructures import AdjListRDD, EdgesRDD
import logging
from operator import add


def fast_embed_svd(coo_list, fun, embed_dims, config=None, phi=None):
    """
    Wrapper for computing SVD embeddings using the FastEmbed class
    :param coo_list: matrix for which we seek an SVD embedding as a COO format RDD
            [(Row name (hashable), Column name (hashable), Entry (float))]
            Row and columns must have unique names like 'Row:0', 'Row:1', 'Col:0', 'Col1', etc
    :param fun: How to weight singular vectors relative to their singular value / maximum singular value
    :param embed_dims: Size of embedding
    :param config: ***** Optional **** configuration parameters - a dict can take keys in:
        'poly_order' - What order of the polynomial approximation approximation of the embedding function 'fun' is
                                needed? Depends on how sharp the function 'fun' is and the size of the matrix. For high
                                dimensional matrices "errors" in approximation can add up. So larger values of 'poly_order'
                                are recommended
        'boost' - How to approximate 'fun'? If nulls in the spectrum are important (which is typically the case when we
                                want to suppress noise), we recommend setting boost to 2 or 3 (as opposed to 1). In this
                                case, we obtain the polynomial approximation of fun(x) ^ (1/boost) and cascade the
                                 algorithm 'boost' number of times. See the paper in the README for details
        'sigma_max' - The spectral norm of the matrix 'mat' computed elsewhere. If this parameter is specified we do not
                                attempt to estimate the spectral norm of the matrix and hence the parameters
                                'spec_norm_est_dims', 'spec_norm_est_iter' and 'scale_up_spec_norm_est' are unused
        'spec_norm_est_dims' - Number of random starting vectors to use to estimate the spectral norm of the matrix
                                via power iteration
        'spec_norm_est_iter' - Number of iterates of power iteration for estimating the spectral norm (depends on how
                                clustered the singular values are around the spectral norm)
        'scale_up_spec_norm_est' - After estimating the spectral norm using power iteration (this is strictly a lower
                                bound on the spectral norm), we multiply it by a small number greater than 1 given by
                                scale_up_spec_norm_est (defaults to FastEmbed.SCALE_UP_SPEC_NORM_EST) to potentially
                                arrive at an upper bound. Since the algorithm assumes that we have an upper bound on the
                                spectral norm of the matrix, this parameter alongside 'spec_norm_est_dims',
                                'spec_norm_est_iter' may have to be tuned if the algorithm diverges.
                                Another option in those cases is to provide the spectral norm computed elsewhere
                                using the parameter sigma_max
        'n_jobs' - Number of cores to use in embedding computation (defaults to -1, which denotes all available cores)
        'normalize_fun' - if set to True (default), fun represents the scaling of the singular value relative to the
                                spectral norm (largest magnitude singular value) ie,
                                f(singularvector/spectralnorm) * singularvector; if set to False, we use fun as is on
                                the singularvalue to obtain its weight ie., f(singularvalue) * singularvector
         'verbose' - if set to True - prints details along the way; Defaults to False
    :param phi **** Optional ****
        how to weight polynomial approximation errors
        integrate(phi(x) * (fun(x) - poly_approx_of_fun(x))^2, -1, 1) (Defaults to phi(x) = 1.)
    :return: a tuple with embedding of (rows, cols) of size (#rows, embed_dims) and (#columns, embed_dims)
            Each entry of the resulting RDDs take the format (Row key/ Column key, embedding vector)
    """

    mat = AdjListRDD().fromUvw(coo_list)

    num_parts = mat.numPartitions

    row_keys = mat.getRowNames().map(lambda x: (x, 1))
    col_keys = mat.getColumnNames().map(lambda x: (x, 1))

    # unique keys needed for this to be meaningful
    sym_mat = AdjListRDD(
        mat._rddT.union(mat._rdd).
            repartition(num_parts)
    )

    if row_keys.join(col_keys).count() > 0:
        raise Exception("Rows and columns of the matrix must have unique names like "
                        "'Row:0', 'Row:1', 'Col:0', 'Col1',  OR word, doc_id")

    def fun_prime(x):
        return fun(x) * (x >= 0) - fun(-x) * (x < 0)

    logging.debug(msg="Computing SVD embedding")
    sym_embed = FastEmbed(sym_mat, fun_prime, embed_dims, config, phi)

    logging.debug(msg="Splitting embedding")
    embed_rows = sym_embed.embed._rdd.join(row_keys).mapValues(lambda x: x[0])
    embed_cols = sym_embed.embed._rdd.join(col_keys).mapValues(lambda x: x[0])

    sum_rows = embed_rows.values().map(lambda x: (x * x).sum()).reduce(add) / embed_dims
    sum_cols = embed_cols.values().map(lambda x: (x * x).sum()).reduce(add) / embed_dims
    logging.debug(msg="Energy in row and col embeds: {} {}".format(sum_rows, sum_cols))

    return embed_rows, embed_cols


def fast_embed_eig(coo_list, fun, embed_dims, config=None, phi=None):
    """
    Wrapper for computing spectral decomposition of symmetric matrices using the FastEmbed class
    :param coo_list: matrix for which we seek an embedding as a COO format RDD
            [(Row name (hashable), Column name (hashable), Entry (float))]
            We assume that the matrix is symmetric
    :param fun: How to weight eigen vectors relative to their eigen value / maximum abs(eigen value)
    :param embed_dims: Size of embedding
    :param config: ***** Optional **** configuration parameters - a dict can take keys in:
        'poly_order' - What order of the polynomial approximation approximation of the embedding function 'fun' is
                                needed? Depends on how sharp the function 'fun' is and the size of the matrix. For high
                                dimensional matrices "errors" in approximation can add up. So larger values of 'poly_order'
                                are recommended
        'boost' - How to approximate 'fun'? If nulls in the spectrum are important (which is typically the case when we
                                want to suppress noise), we recommend setting boost to 2 or 3 (as opposed to 1). In this
                                case, we obtain the polynomial approximation of fun(x) ^ (1/boost) and cascade the
                                 algorithm 'boost' number of times. See the paper in the README for details
        'sigma_max' - The spectral norm of the matrix 'mat' computed elsewhere. If this parameter is specified we do not
                                attempt to estimate the spectral norm of the matrix and hence the parameters
                                'spec_norm_est_dims', 'spec_norm_est_iter' and 'scale_up_spec_norm_est' are unused
        'spec_norm_est_dims' - Number of random starting vectors to use to estimate the spectral norm of the matrix
                                via power iteration
        'spec_norm_est_iter' - Number of iterates of power iteration for estimating the spectral norm (depends on how
                                clustered the singular values are around the spectral norm)
        'scale_up_spec_norm_est' - After estimating the spectral norm using power iteration (this is strictly a lower
                                bound on the spectral norm), we multiply it by a small number greater than 1 given by
                                scale_up_spec_norm_est (defaults to FastEmbed.SCALE_UP_SPEC_NORM_EST) to potentially
                                arrive at an upper bound. Since the algorithm assumes that we have an upper bound on the
                                spectral norm of the matrix, this parameter alongside 'spec_norm_est_dims',
                                'spec_norm_est_iter' may have to be tuned if the algorithm diverges.
                                Another option in those cases is to provide the spectral norm computed elsewhere
                                using the parameter sigma_max
        'n_jobs' - Number of cores to use in embedding computation (defaults to -1, which denotes all available cores)
        'normalize_fun' - if set to True (default), fun represents the scaling of the singular value relative to the
                                spectral norm (largest magnitude singular value) ie,
                                f(singularvector/spectralnorm) * singularvector; if set to False, we use fun as is on
                                the singularvalue to obtain its weight ie., f(singularvalue) * singularvector
         'verbose' - if set to True - prints details along the way; Defaults to False
    :param phi **** Optional ****
        how to weight polynomial approximation errors
        integrate(phi(x) * (fun(x) - poly_approx_of_fun(x))^2, -1, 1) (Defaults to phi(x) = 1.)
    :return: embedding of rows/cols of size (#rows=#cols, embed_dims). Each entry of the result RDD is of the format
            (Row key, embedding vector)
    """

    mat = AdjListRDD().fromEdgesRDD(
        EdgesRDD().fromUvw(coo_list).ensureSymmetric()
    )

    logging.debug(msg="Computing embedding for symmetric matrix")
    fastEmbed = FastEmbed(mat, fun, embed_dims, config, phi)

    energy = fastEmbed.embed._rdd.values().map(lambda x: (x * x).sum()).reduce(add) / embed_dims
    logging.debug(msg="Energy in embedding: {}".format(energy))

    return fastEmbed.embed._rdd

