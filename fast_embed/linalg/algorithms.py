import logging
import sys

logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
loglevel = logging.INFO
logging.basicConfig(level=loglevel, stream=sys.stdout,
                    format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def pca_iter(adjListRDD, tallSkinnyMatrixRDD):
    """
        Core iteration of PCA
    :param adjListRDD: RDD of A
    :param tallSkinnyMatrixRDD: RDD of estimate of dominant column singular vector +
    slack dimensions for speed
    :return:
    """
    from datastructures import TallSkinnyMatrixRDD, AdjListRDD

    if not (isinstance(adjListRDD, AdjListRDD) or isinstance(tallSkinnyMatrixRDD, TallSkinnyMatrixRDD)):
        raise ValueError("Input mismatch")

    import numpy as np
    tallSkinnyMatrixRDD1 = adjListRDD.dot(adjListRDD.transpose().dot(tallSkinnyMatrixRDD))
    outer_prod_mat = tallSkinnyMatrixRDD1.rowOuterProduct()
    logging.debug(msg="PCA outer: {}".format(
        outer_prod_mat.tolist())
    )

    neg_vals, descending_vecs_order = zip(*sorted([(-val, i) for i, val in enumerate(np.diag(outer_prod_mat))]))
    sigma2s = [np.sqrt(-neg_val) if neg_val < 0 else 0 for neg_val in neg_vals]
    logging.debug(msg="PCA rotate: {} {}".format(
        sigma2s, descending_vecs_order)
    )
    reordered_vecs = np.matrix(np.zeros(outer_prod_mat.shape))
    rescale_after_reorder = np.matrix(np.zeros(outer_prod_mat.shape))
    for i, k in enumerate(descending_vecs_order):
        reordered_vecs[k, i] = 1.
        rescale_after_reorder[i, i] = 1. / sigma2s[i] if sigma2s[i] > 0. else 0.

    tallSkinnyMatrixRDD1 = tallSkinnyMatrixRDD1.rotateRows(reordered_vecs). \
        rotateRows(rescale_after_reorder)

    sigma2 = sigma2s[0]
    logging.debug(msg="PCA iter: Lambda max >= {}".format(np.sqrt(sigma2)))
    return tallSkinnyMatrixRDD1


def principal_component(adjListRDD, num_iter, ndims=10, eps=1e-6, checkpoint=False):
    """
    Computes the principal component via power iteration
            (rank one matrix which minimizes || A - sigma u v ^ T ||^2)
    :param a: RDD of A in Adj list format
    :param num_iter: number of power iterations
    :param ndims: slack dims used to accelerate spectral norm estimation
    :param eps: tolerance parameter to decide if we can stop early
    :param checkpoint: (boolean) whether or not we checkpoint intermediate rdds
            (set spark_context.setCheckpointDir() before enabling this)
    :return: A tuple of sigma, u, v^T
    """
    from operator import add
    import numpy as np
    from datastructures import TallSkinnyMatrixRDD, VectorRDD

    if num_iter <= 0:
        raise ValueError('num_iter parameter must be a positive integer')

    n, _ = adjListRDD.size()
    tallSkinnyMatrixRDD = TallSkinnyMatrixRDD().setRandom(
        rowIdxRDD=adjListRDD.getRowNames(),
        ncols=ndims,
        scale=1. / np.sqrt(n)
    )
    for iter in range(num_iter):
        tallSkinnyMatrixRDD = pca_iter(
            adjListRDD,
            tallSkinnyMatrixRDD
        )
        if checkpoint and iter % 10 == 9:
            tallSkinnyMatrixRDD.checkpoint()
        v1 = VectorRDD(
            rdd=tallSkinnyMatrixRDD._rdd.mapValues(
                lambda x: np.squeeze(np.array(x[:, 0].transpose()))
            ),
            numPartitions=tallSkinnyMatrixRDD.numPartitions
        )
        v2 = adjListRDD.transpose().dot(v1)
        sigma = np.sqrt(v2._rdd.values().map(lambda x: np.power(x, 2)).reduce(add))
        if iter >= 1 and abs(sigma * 1. / sigma_prev - 1.) < eps:
            break
        sigma_prev = sigma

    return sigma, v1, v2.dot(1. / sigma)


def compute_embed(mat, mat_spec_norm, embed_dims, boost, lanczos_coeff, alpha, beta,
                  storage_level=None, checkpoint=False):
    """
    Worker function to computes fast_embed embedding
    :param mat: square matrix symmetric matrix in adj list format
    :param mat_spec_norm: Spectral norm of uvw
    :param embed_dims: number of dims for embedding
    :param boost: parameter
    :param lanczos_coeff: polynomial recursion basis
    :param alpha: polynomial recursion coeffs
    :param beta: polynomial recursion coeffs
    :param checkpoint: (boolean) whether or not we checkpoint intermediate rdds
            (set spark_context.setCheckpointDir() before enabling this)
    :return: (projection matrix, embedding) tuple
    """
    from pyspark import StorageLevel
    import numpy as np
    from operator import add
    from collections import deque
    from datastructures import TallSkinnyMatrixRDD, AdjListRDD

    if boost <= 0:
        raise ValueError('boost parameter must be a positive integer')

    if not isinstance(mat, AdjListRDD):
        raise ValueError("input mismatch")

    storage_level = StorageLevel.MEMORY_AND_DISK if storage_level is None else storage_level

    # core lanczos iteration
    def update_lanczos(i, x_i, x_i_minus_1):
        # return (mat*x_i/mat_spec_norm - alpha[i-1]*x_i - beta[i-1]*x_i_minus_1)/beta[i]

        a = mat.dot(x_i).dot(1. / (mat_spec_norm * beta[i])).add(x_i_minus_1.dot(-1. * beta[i - 1] / beta[i]))
        if np.abs(alpha[i - 1] - 0.) < 1e-9:
            return a
        return a.add(x_i.dot(-1. * alpha[i - 1] / beta[i]))

    proj_mat = TallSkinnyMatrixRDD().setRandom(
        mat.getColumnNames(), ncols=embed_dims, scale=1. / np.sqrt(embed_dims)
    )
    proj_mat_boost = proj_mat.copy().persist(storage_level)
    embedding = TallSkinnyMatrixRDD().setZeros(mat.getColumnNames(), embed_dims)

    for boost_idx in range(boost):
        lanc_projs = deque([])
        # recurse to r-th order polynomial approximation of spectral embedding
        for r in range(len(lanczos_coeff)):
            if r == 0:
                lanc_projs.extend([proj_mat_boost.dot(0.), proj_mat_boost.dot(1. / beta[0])])
                embedding = lanc_projs[-1].dot(lanczos_coeff[r])  # reset accumulator
            else:
                lanc_projs.append(update_lanczos(r, lanc_projs[-1], lanc_projs[-2]))
                embedding = embedding.add(lanc_projs[-1].dot(lanczos_coeff[r]))

            iter = boost_idx * len(lanczos_coeff) + r
            if checkpoint and iter % 10 == 9:
                lanc_projs[-2].checkpoint()
                lanc_projs[-1].checkpoint()
                embedding.checkpoint()

            energy_lanczos = lanc_projs[-1]._rdd.values().map(lambda x: np.square(x).sum()).reduce(add) / embed_dims
            energy_embedding = embedding._rdd.values().map(lambda x: np.square(x).sum()).reduce(add) / embed_dims
            logging.debug(msg="Boost {}; Lanczos {}; Energy Lanczos {}; Energy embedding {}".format(
                boost_idx + 1, r, energy_lanczos, energy_embedding))

        embedding = embedding.persist(storage_level)
        # cascading of "spectral shaping"
        proj_mat_boost = embedding.copy().persist(storage_level)

    return proj_mat, embedding


def normalize_adj_matrix(adjListMat):
    """
    Computes the normalized adjacency matrix of a graph given its adjacency matrix
    :param adjListMat: Adjacency matrix in Adj list format (RDD)
    :return: Normalized adjacency matrix in Adj list format (RDD) Dr^-0.5 A Dc^-0.5 used for
            random walk based properties
            (Dr is a diagonal matrix with row sums on the diagonal and
             Dc is a diagonal matrix with column sums on the diagonal)
    """
    from operator import itemgetter
    import math
    from datastructures import VectorRDD, AdjListRDD, DiagonalMatrixRDD

    if not isinstance(adjListMat, AdjListRDD):
        raise ValueError("Unsupported input format")

    degreeRDD1 = adjListMat._rdd.mapValues(lambda x: sum(map(itemgetter(1), x)))
    # VectorRDD with 1/sqrt(degree[node]) as entries
    diagMatrix1 = DiagonalMatrixRDD(
        VectorRDD(degreeRDD1.mapValues(lambda x: math.pow(x, -0.5) if x > 0.0 else 0.0))
    )

    degreeRDD2 = adjListMat._rddT.mapValues(lambda x: sum(map(itemgetter(1), x)))
    # VectorRDD with 1/sqrt(degree[node]) as entries
    diagMatrix2 = DiagonalMatrixRDD(
        VectorRDD(degreeRDD2.mapValues(lambda x: math.pow(x, -0.5) if x > 0.0 else 0.0))
    )
    # w(u,v) / sqrt(degree(u) degree(v))
    return diagMatrix1.preMultiply(diagMatrix2.postMultiply(adjListMat))


def rw_probability_transition_matrix(adjListMat):
    """
    Computes the RW prob transition matrix of a directed graph given its adjacency matrix
    :param adjListMat: Adjacency matrix in Adj list format (RDD)
    :return: Normalized adjacency matrix in Adj list format Dr^-1 (RDD) A used for
            random walk based properties
            (Dr is a diagonal matrix with row sums on the diagonal)
    """
    from operator import itemgetter
    from datastructures import VectorRDD, AdjListRDD, DiagonalMatrixRDD

    if not isinstance(adjListMat, AdjListRDD):
        raise ValueError("Unsupported input format")

    degreeRDD1 = adjListMat._rdd.mapValues(lambda x: sum(map(itemgetter(1), x)))
    # VectorRDD with 1/(degree[node]) as entries
    diagMatrix1 = DiagonalMatrixRDD(
        VectorRDD(degreeRDD1.mapValues(lambda x: 1. / x if x > 0.0 else 0.0))
    )

    # w(u,v) / degree(u)
    return diagMatrix1.preMultiply(adjListMat)
