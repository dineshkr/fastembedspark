"""
Note: Matrix operations used in recursions must guarantee #(input partitions) = #(output partitions)
      for stable recursion on spark. Please enforce this contract
"""


class AdjListRDD:
    """
    Adjacency list format for a matrix of (u, [(v1, w1), ....]) tuples
    """

    def __init__(self, rdd=None, rddT=None, numPartitions=None):
        self._rdd = rdd
        self._rddT = rddT
        self.numPartitions = numPartitions
        self.__inferOtherParameters()

    def __inferOtherParameters(self):
        """
        Fill unset parameters
        :return:
        """
        if self._rdd is None and self._rddT is None:
            return
        if self._rdd is not None and self._rddT is None:
            from operator import add
            self._rddT = self._rdd. \
                flatMap(lambda x: [(v, [(x[0], w)]) for v, w in x[1]]). \
                reduceByKey(add)
        if self._rddT is not None and self._rdd is None:
            from operator import add
            self._rdd = self._rddT. \
                flatMap(lambda x: [(u, [(x[0], w)]) for u, w in x[1]]). \
                reduceByKey(add)
        if self.numPartitions is None and self._rdd is not None:
            self.numPartitions = self._rdd.getNumPartitions()
        if self.numPartitions is None and self._rddT is not None:
            self.numPartitions = self._rddT.getNumPartitions()

    def fromEdgesRDD(self, edgesRDD):
        """
        Constructor
        :param edgesRDD: EdgesRDD
        :return: self
        """
        from operator import add
        if not isinstance(edgesRDD, EdgesRDD):
            raise ValueError("Invalid input exception")
        self.numPartitions = edgesRDD.numPartitions
        self._rdd = edgesRDD._rdd. \
            map(lambda tup: (tup[0][0], [(tup[0][1], tup[1])])).reduceByKey(add)
        self._rddT = edgesRDD._rdd. \
            map(lambda tup: (tup[0][1], [(tup[0][0], tup[1])])).reduceByKey(add)
        return self

    def dot(self, otherRDD):
        """
        Matrix multiplication
        :param otherRDD: TallSkinnyMatrixRDD, VectorRDD, int, float
        :return:
        """
        if isinstance(otherRDD, TallSkinnyMatrixRDD) or isinstance(otherRDD, VectorRDD):
            return self.__multiply__(otherRDD)
        if isinstance(otherRDD, int) or isinstance(otherRDD, float):
            return self.__multiply_constant(otherRDD)
        raise NotImplementedError

    def __multiply__(self, matrix):
        """
        Matrix multiplication with VectorRDD or TallSkinnyMatrixRDD
        :param matrix:
        :return:
        """
        if not (isinstance(matrix, TallSkinnyMatrixRDD) or isinstance(matrix, VectorRDD)):
            raise ValueError("Invalid input exception {}".format(matrix))

        from operator import add

        def mat_mult(tup):
            u_w, val = tup
            return [(u, w * val) for u, w in u_w]

        resultRdd = self._rddT.join(matrix._rdd). \
            values(). \
            flatMap(mat_mult). \
            reduceByKey(add). \
            repartition(matrix.numPartitions)

        if isinstance(matrix, TallSkinnyMatrixRDD):
            return TallSkinnyMatrixRDD(resultRdd, matrix.ncols, matrix.numPartitions)
        return VectorRDD(resultRdd, matrix.numPartitions)

    def __multiply_constant(self, constant):
        """
        Scale this matrix by the constant
        :param constant:
        :return:
        """
        resultRdd = AdjListRDD()
        resultRdd._rdd = self._rdd.mapValues(lambda x: map(lambda y: (y[0], constant * y[1]), x))
        resultRdd._rddT = self._rddT.mapValues(lambda x: map(lambda y: (y[0], constant * y[1]), x))
        resultRdd.numPartitions = self.numPartitions
        return resultRdd

    def transpose(self):
        """
        Return the transpose of this matrix
        :return:
        """
        transposed = AdjListRDD()
        transposed._rddT = self._rdd
        transposed._rdd = self._rddT
        return transposed

    def getRowNames(self):
        """

        :return: Names of the row features as an RDD
        """
        return self._rdd.keys()

    def getColumnNames(self):
        """

        :return: Names of the column features as an RDD
        """
        return self._rddT.keys()

    def size(self):
        """

        :return: A tuple (Number of rows, number of columns)
        """
        return self.getRowNames().count(), self.getColumnNames().count()

    def fromUvw(self, uvwRDD):
        """
        Convert a COO format RDD (row index, column index, value) to the AdjListRDD format
        :param uvwRDD: List of edges (row key, column key, value format)
        :return:
        """
        return AdjListRDD().fromEdgesRDD(EdgesRDD().fromUvw(uvwRDD))

    def toUvw(self):
        """

        :return: COO format raw RDD (row index, column index, value)
        """
        return self.toEdgesRDD().toUvw()

    def toEdgesRDD(self):
        """

        :return: COO format RDD ((row index, column index), value) but wrapped by the EdgesRDD class
        """
        return EdgesRDD(
            rdd=self._rdd.flatMap(lambda x: [((x[0], v), w) for v, w in x[1]])
        )


class EdgesRDD:
    """
    COO format
    """

    def __init__(self, rdd=None, numPartitions=None):
        self._rdd = rdd
        self.numPartitions = numPartitions
        self.__inferOtherParameters()

    def __inferOtherParameters(self):
        """
        Fill numPartitions
        :return:
        """
        if self._rdd is None:
            return
        if self.numPartitions is None:
            self.numPartitions = self._rdd.getNumPartitions()

    def fromTextFile(self, spark_context, file_name, separator='\t', numPartitions=1000):
        """

        :param spark_context: SparkContext
        :param file_name: hdfs / local file
        :param separator: separator for reading line in (row key, column key, value format)
        :param numPartitions:
        :return: self
        """
        from operator import add
        self.numPartitions = numPartitions
        self._rdd = spark_context.textFile(file_name, self.numPartitions). \
            map(lambda line: line.split(separator)).filter(lambda tup: len(tup) == 3). \
            map(lambda tup: ((tup[0], tup[1]), float(tup[2]))). \
            reduceByKey(add)
        return self

    def fromUvw(self, uvwRDD):
        """
        Wrap a COO format RDD (row index, column index, value) by the EdgesRDD class
        :param uvwRDD: List of edges (row key, column key, value format)
        :return:
        """
        from operator import add
        self._rdd = uvwRDD. \
            map(lambda tup: ((tup[0], tup[1]), tup[2])). \
            reduceByKey(add)
        self.__inferOtherParameters()
        return self

    def toUvw(self):
        """

        :return: COO format raw RDD (row index, column index, value)
        """
        return self._rdd.map(lambda x: (x[0][0], x[0][1], x[1]))

    def transpose(self):
        """
        A^T
        :return:
        """
        return EdgesRDD(
            rdd=self._rdd.map(lambda tup: ((tup[0][1], tup[0][0]), tup[1])),
            numPartitions=self.numPartitions
        )

    def ensureSymmetric(self):
        """
        (A + A^T) / 2.0
        :return:
        """
        from operator import add
        return EdgesRDD(
            rdd=self._rdd.
                flatMap(lambda tup: [(tup[0], tup[1]), (tuple(reversed(tup[0])), tup[1])]).
                reduceByKey(add).mapValues(lambda x: x * 0.5),
            numPartitions=self.numPartitions
        )

    def makeUnWeighted(self):
        """
        Set all edge weights to one
        :return:
        """
        return EdgesRDD(
            rdd=self.ensureSymmetric()._rdd.mapValues(lambda _: 1.),
            numPartitions=self.numPartitions
        )


class VectorRDD:
    """
    A distributed sparse vector
    """

    def __init__(self, rdd=None, numPartitions=None):
        self._rdd = rdd
        self.numPartitions = numPartitions
        self.__inferOtherParameters()

    def __inferOtherParameters(self):
        if self._rdd is None:
            return
        if self.numPartitions is None:
            self.numPartitions = self._rdd.getNumPartitions()

    def setRandom(self, rowIdxRDD, scale=1):
        """
        Generate a random vector
        :param rowIdxRDD: Row names
        :param scale: max value
        :return:
        """
        self.numPartitions = rowIdxRDD.getNumPartitions()

        def _rand():
            import time
            import random
            random.seed(int(time.time() * 1e6) % 4294967295)
            return 2. * scale * (random.random() > 0.5) - scale

        self._rdd = rowIdxRDD.distinct().map(lambda x: (x, _rand()))
        return self

    def size(self):
        """

        :return: integer
        """
        return self._rdd.count()

    def add(self, other):
        """

        :param other: VectorRDD
        :return: self + other
        """
        if not isinstance(other, VectorRDD):
            raise ValueError("Input mismatch")

        def _add(x, y):
            if x is None:
                return y
            if y is None:
                return x
            return x + y

        valueRDD = VectorRDD(numPartitions=self.numPartitions)
        valueRDD._rdd = self._rdd.fullOuterJoin(other._rdd).mapValues(lambda x: _add(x[0], x[1])). \
            filter(lambda x: x[1] is not None). \
            repartition(self.numPartitions)
        return valueRDD

    def dot(self, scalar):
        """

        :param scalar:int or float
        :return: scalar times self
        """
        if not (isinstance(scalar, int) or isinstance(scalar, float)):
            raise ValueError("Input mismatch")
        return VectorRDD(
            rdd=self._rdd.mapValues(lambda x: x * scalar),
            numPartitions=self.numPartitions
        )

    def persist(self, storageLevel):
        """
        Persist the underlying RDD
        :param storageLevel: Spark storage level
        :return: self
        """
        self._rdd = self._rdd.persist(storageLevel)
        return self

    def unpersist(self):
        """
        Request Spark to clean cache for this rdd
        :return:
        """
        self._rdd.unpersist()

    def checkpoint(self):
        """
        http://spark.apache.org/docs/2.1.0/api/python/pyspark.html#pyspark.RDD.checkpoint

        Mark this RDD for checkpointing. It will be saved to a file inside the checkpoint
        directory set with SparkContext.setCheckpointDir() and all references to its parent
        RDDs will be removed. This function must be called before any job has been executed
        on this RDD. It is strongly recommended that this RDD is persisted in memory,
        otherwise saving it on a file will require recomputation
        :return: self
        """
        self._rdd.cache()
        self._rdd.checkpoint()
        return self


class TallSkinnyMatrixRDD:
    """
    Dense matrix with many rows and a few columns
    """

    def __init__(self, rdd=None, ncols=None, numPartitions=None):
        self._rdd = rdd
        self.numPartitions = numPartitions
        self.ncols = ncols
        self.__inferOtherParameters()

    def __inferOtherParameters(self):
        if self._rdd is None:
            return
        if self.numPartitions is None:
            self.numPartitions = self._rdd.getNumPartitions()
        if self.ncols is None:
            self.ncols = len(self._rdd.values().first())

    def setRandom(self, rowIdxRDD, ncols, scale=1.):
        """
        Initialize a random instance
        :param rowIdxRDD: names of rows
        :param ncols: number of columns
        :param scale: max value
        :return:
        """
        self.numPartitions = rowIdxRDD.getNumPartitions()
        self.ncols = ncols

        def _rand():
            import time
            import numpy.random
            numpy.random.seed(int(time.time() * 1e6) % 4294967295)
            return 2. * scale * (numpy.random.rand(ncols) > 0.5) - scale

        self._rdd = rowIdxRDD.distinct().map(lambda x: (x, _rand()))
        return self

    def setZeros(self, rowIdxRDD, ncols):
        """

        :param rowIdxRDD: names of rows
        :param ncols: number of columns
        :return:
        """
        import numpy as np
        self.numPartitions = rowIdxRDD.getNumPartitions()
        self.ncols = ncols
        self._rdd = rowIdxRDD.distinct().map(lambda x: (x, np.zeros(ncols)))
        return self

    def size(self):
        """
        Returns the shape of this matrix
        :return: (nrows, ncols)
        """
        return self._rdd.count(), self.ncols

    def add(self, other):
        """
        self + other (assumes absent rows in either of the two rdds
                        are zero vectors; sparse representation)
        :param other: Any VectorRDD with the same number of columns
        :return:
        """
        if not isinstance(other, TallSkinnyMatrixRDD):
            raise ValueError("Input mismatch")

        if not self.ncols == other.ncols:
            raise ValueError("Input mismatch")

        def _add(x, y):
            if x is None:
                return y
            if y is None:
                return x
            return x + y

        valueRDD = TallSkinnyMatrixRDD(ncols=self.ncols, numPartitions=self.numPartitions)
        valueRDD._rdd = self._rdd.fullOuterJoin(other._rdd).mapValues(lambda x: _add(x[0], x[1])). \
            filter(lambda x: x[1] is not None). \
            repartition(self.numPartitions)
        return valueRDD

    def dot(self, scalar):
        """
        Scalar multiplication
        :param scalar: int, float Others not implemented
        :return:
        """
        if not (isinstance(scalar, int) or isinstance(scalar, float)):
            raise ValueError("Input mismatch")
        return TallSkinnyMatrixRDD(
            rdd=self._rdd.mapValues(lambda x: x * scalar),
            ncols=self.ncols,
            numPartitions=self.numPartitions
        )

    def rowOuterProduct(self):
        """

        :return: A^TA
        """
        from operator import add
        import numpy as np

        return self._rdd.values(). \
            map(np.matrix). \
            map(lambda x: (x.transpose()).dot(x)). \
            reduce(add)

    def rotateRows(self, rotationMatrix):
        """

        :param rotationMatrix: A rotation matrix Q
        :return: A times Q
        """
        import numpy as np
        if not isinstance(rotationMatrix, np.matrix):
            raise Exception("Input mismatch")
        return TallSkinnyMatrixRDD(
            rdd=self._rdd.
                mapValues(np.matrix).
                mapValues(lambda x: x.dot(rotationMatrix)).
                mapValues(np.array),
            ncols=self.ncols,
            numPartitions=self.numPartitions
        )

    def persist(self, storageLevel):
        """
        Persist the underlying RDD
        :param storageLevel: Spark storage level
        :return: self
        """
        self._rdd = self._rdd.persist(storageLevel)
        return self

    def unpersist(self):
        """
        Request Spark to clean cache for this rdd
        :return:
        """
        self._rdd.unpersist()

    def checkpoint(self):
        """
        http://spark.apache.org/docs/2.1.0/api/python/pyspark.html#pyspark.RDD.checkpoint

        Mark this RDD for checkpointing. It will be saved to a file inside the checkpoint
        directory set with SparkContext.setCheckpointDir() and all references to its parent
        RDDs will be removed. This function must be called before any job has been executed
        on this RDD. It is strongly recommended that this RDD is persisted in memory,
        otherwise saving it on a file will require recomputation
        :return: self
        """
        self._rdd.cache()
        self._rdd.checkpoint()
        return self

    def copy(self):
        """
        Make a copy of this RDD
        :return: a copy of this RDD
        """
        return TallSkinnyMatrixRDD(
            rdd=self._rdd,
            numPartitions=self.numPartitions,
            ncols=self.ncols
        )


class DiagonalMatrixRDD:
    """
    A diagonal matrix for scaling dimensions.
    Internally we store the leading diagonal as an VectorRDD
    """

    def __init__(self, vectorRDD):
        """

        :param vectorRDD: Leading diagonal
        """
        from operator import add
        if not isinstance(vectorRDD, VectorRDD):
            raise ValueError("Input mismatch")
        self.vectorRdd = vectorRDD
        self.vectorRdd._rdd = self.vectorRdd._rdd.reduceByKey(add)

    def preMultiply(self, adjListMat):
        """
        D times A (scales the rows of adjListMat)
        :param adjListMat: A matrix
            (keys / dimensions are implicitly assumed to be the union)
        :return:
        """
        if not isinstance(adjListMat, AdjListRDD):
            raise ValueError("Input mismatch")

        def scale_column(edges, weight):
            return [(u, w * weight) for u, w in edges]

        rddT = adjListMat._rddT. \
            join(self.vectorRdd._rdd). \
            mapValues(lambda tup: scale_column(*tup)). \
            repartition(adjListMat.numPartitions)

        return AdjListRDD(rddT=rddT, numPartitions=adjListMat.numPartitions)

    def postMultiply(self, adjListMat):
        """
        A times D (scales the columns of adjListMat)
        :param adjListMat: A matrix
                (keys / dimensions are implicitly assumed to be the union)
        :return:
        """
        if not isinstance(adjListMat, AdjListRDD):
            raise ValueError("Input mismatch")

        def scale_row(edges, weight):
            return [(v, w * weight) for v, w in edges]

        rdd = adjListMat._rdd. \
            join(self.vectorRdd._rdd). \
            mapValues(lambda tup: scale_row(*tup)). \
            repartition(adjListMat.numPartitions)

        return AdjListRDD(rdd=rdd, numPartitions=adjListMat.numPartitions)
