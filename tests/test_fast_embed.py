from fast_embed.fast_embed import FastEmbed


class TestFastEmbed:
    # @staticmethod
    # def test_defaults(uvw, fun, embed_dims):
    #     """
    #     Checks if FastEmbed works with default configuration
    #     :param uvw: matrix to embed
    #     :param fun: embedding function
    #     :param embed_dims: dimensionality of compressive embedding
    #     :return: FastEmbed obj
    #     """
    #     embed = FastEmbed(uvw, fun, embed_dims)
    #
    #     # check if correct defaults are getting picked up
    #     assert embed.poly_order == FastEmbed.POLY_ORDER
    #     assert embed.boost == FastEmbed.BOOST
    #     assert embed.scale_up_spec_norm_est == FastEmbed.SCALE_UP_SPEC_NORM_EST
    #     assert embed.spec_norm_est_iter == FastEmbed.SPEC_NORM_EST_ITER
    #     assert embed.normalize_fun == FastEmbed.NORMALIZE_FUN
    #     assert all(v.keys().count() == uvw.keys().count() for v in embed.embed)
    #
    #     return embed
    #
    # @staticmethod
    # def test_custom(uvw, fun, embed_dims, config, phi):
    #     """
    #     Checks if FastEmbed works with custom configurations
    #     :param uvw: matrix to embed
    #     :param fun: embedding function
    #     :param embed_dims: dimensionality of compressive embedding
    #     :param config: configuration parameters
    #     :param phi: function to weight polynomial approximation errors
    #     :return: FastEmbed object
    #     """
    #     embed = FastEmbed(uvw, fun, embed_dims, config, phi)
    #
    #     # check if custom config is getting picked up
    #     assert all(getattr(embed, key) == val for key, val in config.items())
    #     assert all(v.keys().count() == uvw.keys().count() for v in embed.embed)
    #
    #     return embed
    pass
